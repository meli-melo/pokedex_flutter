import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart' as http;

class PokemonList {
  CustomList customlist =CustomList(count: 0, next: null, previous: null, results: new List<PokemonListElement>());

  PokemonList();

  void fetchPokemonList(String url) async {
    log('start loading');
    final response = await http.get(url);

    if(response.statusCode == 200)
    {
      PokemonList temp = PokemonList.fromJson(json.decode(response.body));
      log('results ' + temp.customlist.results.length.toString());
      if(this.customlist == null) {
        this.customlist = CustomList(count: 0, next: null, previous: null, results: null);
      }
      if(this.customlist.results != null) {
        this.customlist.results.addAll(temp.customlist.results);
      }
      else {
        this.customlist.results = temp.customlist.results;
      }
      log('results ' + this.customlist.results.length.toString());
      this.customlist.count += temp.customlist.count;
      this.customlist.next = temp.customlist.next;
      this.customlist.previous = temp.customlist.previous;
      log('finish loading');
      return;
    }
    else
    {
      throw Exception('Failed to load Pokemon list');
    }
  }

  factory PokemonList.fromJson(Map<String, dynamic> json) {
    var list = json['results'] as List;
    List<PokemonListElement> elementList = list.map((element) => PokemonListElement.fromJson(element)).toList();
    CustomList clist = CustomList(
      count: json['count'],
      next: json['next'],
      previous: json['previous'],
      results: elementList,
    );

    PokemonList res = PokemonList();
    res.customlist = clist;
    return res;
  }
}

class CustomList {
  int count;
  String next;
  String previous;
  List<PokemonListElement> results;

  CustomList({this.count, this.next, this.previous, this.results});
}

class PokemonListElement {
  String name;
  String url;

  PokemonListElement({this.name, this.url});

  factory PokemonListElement.fromJson(Map<String, dynamic> json) {
    return PokemonListElement(
      name: json['name'], 
      url: json['url']
    );
  }
}