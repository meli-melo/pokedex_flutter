import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;

const String baseUrl = 'https://pokeapi.co/api/v2/';

class Pokemon {
  int id;
  String name;
  int baseExperience;
  int height;
  bool isDefault;
  int order;
  int weight;
  // List<Ability> abilities;
  // List<Form> forms;

  Pokemon({this.id, this.name, this.baseExperience, this.height, this.isDefault, this.order, this.weight});

  static Future<Pokemon> fetchPokemon() async {
  final response = await http.get(baseUrl + 'pokemon/1');

  if(response.statusCode == 200) {
    return Pokemon.fromJson(json.decode((response.body)));
  }
  else {
    throw Exception('Failed to load album');
  }
}

  factory Pokemon.fromJson(Map<String, dynamic> json) {
    return Pokemon(
      id: json['id'],
      name: json['name'],
      baseExperience: json['base_experience'],
      height: json['height'],
      isDefault: json['is_default'],
      order: json['order'],
      weight: json['weight'],
    );
  }
}

class Form {
  String name;
  String url;

  Form(this.name, this.url);
}

class Ability {
  bool isHidden;
  int slot;
  AbilityDetails ability;
}

class AbilityDetails{
  String name;
  String url;
}