import 'dart:async';
import 'dart:developer';



import 'package:flutter/material.dart';
import 'models/pokemon.dart';
import 'models/pokemon_list.dart';

const String baseUrl = 'https://pokeapi.co/api/v2/';

void main() => runApp(MyApp());


class MyApp extends StatefulWidget {
  //MyApp({Key key}) : super(key: key);

  @override 
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  PokemonList futurePokemonList;
  ScrollController _controller = new ScrollController();

  @override 
  void initState() {
    super.initState();
    futurePokemonList = PokemonList();
    log('init load');
    futurePokemonList.fetchPokemonList(baseUrl + 'pokemon');

    _controller.addListener(() {
      if(_controller.position.pixels == _controller.position.maxScrollExtent)
      {
        log('next load');
        futurePokemonList.fetchPokemonList(futurePokemonList.customlist.next);        
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Pokedex'),
        ),
        body: Container (
          child: _buildListView(),
        )
      )
    );
  }

  Widget _buildList() {
    return FutureBuilder(
      builder: (context, snapshot) {
        if(snapshot.hasData)
        {
          //return _buildListView(snapshot);
        }
        else if(snapshot.hasError) {
          return Text("${snapshot.error}");
        }
        return CircularProgressIndicator();
      }
    );
  }

  

  Widget _buildRow(PokemonListElement res) {
    log('build row');
    return ListTile(
      title: Text(
        res.name,
        style: TextStyle(fontSize: 18.0)
      ),
    );
  }

  Widget _buildListView()
  {
    
    ListView list = ListView.builder (
      padding: const EdgeInsets.all(16.0),
      itemBuilder: (context, i){
        if(i == futurePokemonList.customlist.count){
          log('load circle ' + i.toString());
          log('load circle ' + futurePokemonList.customlist.count.toString());
          return CircularProgressIndicator();
        }
        else {
          //if(i.isOdd) return Divider();
          log('test ' + i.toString());
          log('length ' + futurePokemonList.customlist.count.toString());
          return _buildRow(futurePokemonList.customlist.results[i]);
        }
      },
      itemCount: futurePokemonList.customlist.count + 1,
    );

    return list;
  }

  void _onPokemonTap(BuildContext context, String url)
  {
    
  }
}