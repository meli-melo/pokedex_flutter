import 'package:flutter/material.dart';

import '../models/pokemon_list.dart';

class PokemonListElementWidget extends StatelessWidget {
  final PokemonListElement pokemon;

  const PokemonListElementWidget({
    Key key,
    @required this.pokemon,
  }) : assert(pokemon != null),
        super(key : key);

  @override
  Widget build(BuildContext context)
  {
    return ListTile(
      title: Text(pokemon.name),
    );
  }
}