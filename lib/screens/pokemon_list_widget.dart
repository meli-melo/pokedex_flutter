// import 'package:flutter/material.dart';
// import '../models/pokemon_list.dart';
// import 'pokemon_list_element_widget.dart';

// class PokemonListWidget extends StatefulWidget {
//   PokemonListWidget({Key key}) : super(key:key);

//   @override
//    _PokemonListWidgetState createState() => _PokemonListWidgetState();

// }

// class _PokemonListWidgetState extends State<PokemonListWidget> {
//   final scrollController = ScrollController();
//   PokemonList pokemonList;

//   @override 
//   void initState() {
//     super.initState();
//     pokemonList = PokemonList();
//     scrollController.addListener(() {
//       if(scrollController.position.maxScrollExtent == scrollController.offset){
//         pokemonList.loadNext();
//       }
//     });
//   }

//   @override 
//   Widget build(BuildContext context) {
//     return StreamBuilder(
//       stream: pokemonList.stream,
//       builder: (BuildContext context, AsyncSnapshot<PokemonList> _snapshot) {
//         if(!_snapshot.hasData) {
//           return Center(child: CircularProgressIndicator(),);
//         }
//         else {
//           return RefreshIndicator(
//             onRefresh: null,//pokemonList.refresh,
//             child: ListView.separated(
//               padding: EdgeInsets.symmetric(vertical: 8.0),
//               controller: scrollController,
//               separatorBuilder: (context, index) => Divider(),
//               itemCount: _snapshot.data.count + 1,
//               itemBuilder: (BuildContext _context, int index) {
//                 if(index < _snapshot.data.count){
//                   return PokemonListElementWidget(
//                     pokemon: PokemonListElement(
//                       name: _snapshot.data.results[index].name,
//                       url: _snapshot.data.results[index].url
//                     )
//                   );
//                 }
//                 else if(pokemonList.next != ""){
//                   return Padding(padding: EdgeInsets.symmetric(vertical: 32.0),
//                   child: Center(child:CircularProgressIndicator()),
//                   );
//                 }
//                 else {
//                   return Padding(
//                     padding: EdgeInsets.symmetric(vertical: 32.0),
//                     child: Center(child:Text('no more')),
//                   );
//                 }
//               }, 
//             )
//           );
//         }
//       },
//     );
//   }
// }